![](public/img/og.jpg)

https://bizdamienlu.gitlab.io/umc_202206/

# umc_202206

## Project setup
npm version 16.13.2

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
