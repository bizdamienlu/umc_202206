import Vue from "vue";
import VueMeta from "vue-meta";
import router from "./router";

import VueScrollactive from "vue-scrollactive";

import AOS from "aos";
import "aos/dist/aos.css";
import "@/assets/scss/main.scss";

import App from "./App.vue";

Vue.config.productionTip = false;
Vue.use(VueMeta);
Vue.use(VueScrollactive);

new Vue({
  router,
  render: (h) => h(App),
  created() {
    AOS.init({
      duration: 500,
      easing: "cubic-bezier(.48,.01,.52,.99)",
      anchorPlacement: "top-center",
      offset: -100,
      once: true,
    });
  },
  mounted() {
    document.dispatchEvent(new Event("render-event"));
  },
}).$mount("#app");
